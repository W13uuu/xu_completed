from flask import Flask,jsonify,request
import json
from flask_cors import *
from backend import *

app =Flask(__name__)
CORS(app,resources=r'/*')
@app.route('/avg_month',methods=['GET'])
def avg_month():
    data = pd.read_excel('data/avg_month.xlsx','Sheet1')
   
    param1 = request.args.get('param1')
    # print(param1)
    data = concatData(remove(data),param1)
    print(data)
    return json.dumps(data)

@app.route('/monthly_data',methods=['GET'])
def monthly_data():
    data = pd.read_excel('data/monthly_data.xlsx','Sheet1')
    param1 = request.args.get('param1')
    # print(param1)
    data1 = remove2(data,param1)
    print(data1)
    return json.dumps(data1)

@app.route('/pearson',methods=['GET'])
def pearson():
    data_monthly = pd.read_excel('data/monthly_data.xlsx','Sheet1')
    data_avg_month = pd.read_excel('data/avg_month.xlsx','Sheet1')
    # param1 = request.args.get('param1')
    # print(param1)
    data1 = getPearson(data_monthly,data_avg_month)
    print(data1)
    return json.dumps(data1)


app.run()