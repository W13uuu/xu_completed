function toggleSubMenu(submenuNumber) {
  var submenu = document.getElementById("submenu" + submenuNumber);
  submenu.classList.toggle("active");
}

// function getData() {

//   const Http = new XMLHttpRequest();
//   const url = "http://127.0.0.1:5000";
//   var response;
//   Http.open("GET", url);
//   Http.onreadystatechange = function () {
//     if (Http.readyState === 4 && Http.status === 200) {
//       response = JSON.parse(Http.responseText);

//     }
//   };
//   Http.send();

//   // Http.onreadystatechange = (e) => {
//   //   // console.log(Http.responseText);
//   //   var tmp = JSON.parse(Http.responseText);
//   //   // var name = "USD/CNY";
//   //   return tmp;
//   // };
// }

async function fetchData(url) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          console.log(xhr.responseText);
          var response = JSON.parse(xhr.responseText);
          resolve(response); // 解析 Promise
        } else {
          reject(xhr.statusText); // 拒绝 Promise
        }
      }
    };
    xhr.send();
  });
}

async function test() {
  var requests = [];
  url = "http://127.0.0.1:5000/avg_month?param1=";
  for (var i = 0; i <= 5; i++) {
    requests.push(fetchData(url + (2018 + i)));
  }
  try {
    var tmp1 = await Promise.all(requests); // 等待异步操作完成
    for (var i = 1; i <= 6; i++) {
      // 基于准备好的dom，初始化echarts实例
      var myChart = echarts.init(document.getElementById("chart" + i));
      // var tmp = getData();
      var name = "USD/CNY";
      // console.log(tmp[i]);
      tmp = tmp1[i - 1];
      // 指定图表的配置项和数据
      var option = {
        title: {
          text: 2018 + i - 1, // 设置标题文本
          textStyle: {
            fontSize: 18, // 标题字体大小
            fontWeight: "bold", // 标题字体粗细
          },
        },
        legend: {
          data: ["USD/CNY", "100JPY/CNY", "GBP/CNY", "HKD/CNY", "EUR/CNY"],
        },

        xAxis: {
          type: "category",
          data: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
          ],
        },
        yAxis: {
          type: "value",
        },
        series: [
          {
            name: "USD/CNY",
            data: tmp["USD/CNY"],
            type: "line",
            smooth: true,
          },
          {
            name: "100JPY/CNY",
            data: tmp["100JPY/CNY"],
            type: "line",
            smooth: true,
          },
          {
            name: "GBP/CNY",
            data: tmp["GBP/CNY"],
            type: "line",
            smooth: true,
          },
          {
            name: "HKD/CNY",
            data: tmp["HKD/CNY"],
            type: "line",
            smooth: true,
          },
          {
            name: "EUR/CNY",
            data: tmp["EUR/CNY"],
            type: "line",
            smooth: true,
          },
        ],
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
    }
    // console.log(data); // 处理返回的数据
  } catch (error) {
    console.log(error); // 处理错误
  }
}

async function test2() {
  var requests = [];
  url = "http://127.0.0.1:5000/monthly_data?param1=";
  for (var i = 0; i <= 5; i++) {
    requests.push(fetchData(url + (2018 + i)));
  }
  try {
    var tmp1 = await Promise.all(requests); // 等待异步操作完成
    for (var i = 1; i <= 6; i++) {
      // 基于准备好的dom，初始化echarts实例
      var myChart = echarts.init(document.getElementById("chart" + i));
      // var tmp = getData();
      var name = "USD/CNY";
      // console.log(tmp[i]);
      tmp = tmp1[i - 1];
      // 指定图表的配置项和数据
      option = {
        title: {
          text: 2018 + i - 1, // 设置标题文本
          textStyle: {
            fontSize: 18, // 标题字体大小
            fontWeight: "bold", // 标题字体粗细
          },
        },
        legend: {
          data: [
            "进出口总值当期值(千美元)",
            "出口总值当期值(千美元)",
            "进口总值当期值(千美元)",
          ],
        },

        xAxis: {
          type: "category",
          data: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
          ],
        },
        yAxis: {
          type: "value",
        },
        series: [
          {
            name: "进口总值当期值(千美元)",
            data: tmp["进口总值当期值(千美元)"],
            type: "bar",
            showBackground: true,
            backgroundStyle: {
              color: "rgba(220, 220, 220, 0.8)",
            },
          },
          {
            name: "出口总值当期值(千美元)",
            data: tmp["出口总值当期值(千美元)"],
            type: "bar",
            showBackground: true,
            backgroundStyle: {
              color: "rgba(220, 220, 220, 0.8)",
            },
          },
          {
            name: "进出口总值当期值(千美元)",
            data: tmp["进出口总值当期值(千美元)"],
            type: "bar",
            showBackground: true,
            backgroundStyle: {
              color: "rgba(220, 220, 220, 0.8)",
            },
          },
        ],
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
    }
    // console.log(data); // 处理返回的数据
  } catch (error) {
    console.log(error); // 处理错误
  }
}

async function test3() {
  var requests = [];
  url = "http://127.0.0.1:5000/pearson";

  try {
    var tmp1 = await fetchData(url); // 等待异步操作完成
    i = 1;
    for (var p in tmp1) {
      // 基于准备好的dom，初始化echarts实例
      var myChart = echarts.init(document.getElementById("chart" + i));
      i++;
      // var tmp = getData();
      // console.log(tmp[i]);
      // 指定图表的配置项和数据
      var option = {
        title: {
          text: p, // 设置标题文本
          textStyle: {
            fontSize: 18, // 标题字体大小
            fontWeight: "bold", // 标题字体粗细
          },
        },

        xAxis: {
          type: "category",
          data: ["2018", "2019", "2020", "2021", "2022", "2023"],
        },
        yAxis: {
          type: "value",
        },
        series: [
          {
            name: p,
            data: tmp1[p],
            type: "line",
            smooth: true,
          },
        ],
      };

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option);
    }
    // console.log(data); // 处理返回的数据
  } catch (error) {
    console.log(error); // 处理错误
  }
}
